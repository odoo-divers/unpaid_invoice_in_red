{
    'name' : 'Unpaid invoice in red color',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'account',
    ],
    'data' : [
    		'unpaid_invoice_in_red_view.xml',
    ],

    'installable' : True,
    'application' : False,
}
